﻿using System;
using System.IO;
using System.Collections.Generic;

namespace prog
{
    class Program
    {
        private const string initFilePath = "storage/init-file.txt";
        private const string newFilePath = "storage/new-file.txt";

        static void Main(string[] args)
        {
            // naplnenie listu desiatimi cislami
            fillInitFile();

            // nacitanie hodnot z textoveho suboru do listu
            List<int> numbersList = loadInitFileData();

            // zapisanie vstupu do stredu listu
            for (int i = 0; i < 3; i++)
            {
                Console.Write("Zadajte {0}. číslo: ", (i + 1));
                int inputNumber = Convert.ToInt32(Console.ReadLine());

                numbersList.Insert((numbersList.Count / 2), inputNumber);
            }
            
            Console.WriteLine("List s pridanými hodnotami: ");
            numbersList.ForEach(number => Console.Write("{0}\n", number));
            Console.WriteLine();

            // vymazanie prvku pola
            Console.Write("Zadajte pozíciu, na ktorej sa má vymazať prvok listu: ");
            int listPos = Convert.ToInt32(Console.ReadLine());
            numbersList.RemoveAt(listPos);

            Console.WriteLine("List s vymazaným prvkom: ");
            numbersList.ForEach(number => Console.Write("{0}\n", number));
            Console.WriteLine();

            // reverse listu
            numbersList.Reverse();

            Console.WriteLine("Reverse-nutý list: ");
            numbersList.ForEach(number => Console.Write("{0}\n", number));
            Console.WriteLine();

            // vytvorenie noveho suboru a zapisanie listu don
            newFile(numbersList);

            Console.WriteLine("Upravený list sa uložil do nového súboru {0}", newFilePath);
        }

        private static void fillInitFile()
        {
            var numbersList = new List<int>() {83, 4, 5, 20, 10, 18, 49, 50, 77, 2};

            numbersList.ForEach(number => File.AppendAllText(initFilePath, number + Environment.NewLine));

            Console.WriteLine("Pôvodný list: ");
            numbersList.ForEach(number => Console.Write("{0}\n", number));
            Console.WriteLine();
        }

        private static List<int> loadInitFileData()
        {
            string[] fileData = File.ReadAllLines(initFilePath);
            
            var numbersList = new List<int>();

            foreach (string line in fileData)
            {
                numbersList.Add(Convert.ToInt32(line));
            }
            
            return numbersList;
        }

        private static void newFile(List<int> numbersList)
        {
            numbersList.ForEach(number => File.AppendAllText(newFilePath, number + Environment.NewLine));
        }
    }
}
